﻿# TODO: Translation updated at 2020-04-26 23:31

# game/script.rpy:154
translate spanish basic_defeat_e732ae62:

    # q "FUCKING RAT!!!"
    q "¡¡¡MALDITA RATA!!!"

# game/script.rpy:156
translate spanish basic_defeat_31ebcd2e:

    # q "I should never have come here..."
    q "Nunca debí haber venido aquí..."

# game/script.rpy:158
translate spanish basic_defeat_c09fc562:

    # n "{b}GAME OVER{/b}"
    n "{b}GAME OVER{/b}"

# game/script.rpy:173
translate spanish start_c6298d52:

    # q "After the worldwide success that was K/Da, Akali decided to search and recruit the best musicians to form a new group, True Damage."
    q "Después del éxito mundial que fue K/Da, Akali decidió buscar y reclutar a los mejores músicos para formar un nuevo grupo, True Damage."

# game/script.rpy:175
translate spanish start_558285aa:

    # q "I have to admit that Akali, Ekko, Senna and Yasuo are quite talented artists but, you know, they’re nothing without me, la hermosa princesa Qiyana."
    q "Tengo que admitir que Akali, Ekko, Senna y Yasuo son artistas bastante talentosos pero, ya sabes, no son nada sin mí, la hermosa princesa Qiyana."

# game/script.rpy:195
translate spanish start_2ab5832e:

    # q "And as you expected, I'm not here to talk about wasted talents, I'm here to talk about me, a goddess among you."
    q "Pero regresando al tema, no estoy aquí para hablar de talentos desperdiciados, estoy aquí para hablar de mí, una diosa entre ustedes..."

# game/script.rpy:203
translate spanish start_761e3824:

    # q "Our manager, Teemo, an irrelevant creep. I really hate that piece of shit..."
    q "Nuestro manager, Teemo, es un imbécil irrelevante. Realmente odio ese pedazo de mierda..."

# game/script.rpy:208
translate spanish start_b72cbd5d:

    # q "I have no time to explain why I’m still holding up that guy..."
    q "No tengo tiempo para explicar por qué sigo soportando a ese tipo..."

# game/script.rpy:214
translate spanish start_d267661a:

    # q "..."
    q "..."

# game/script.rpy:222
translate spanish start_83b1c362:

    # q "He said we have something very important to talk about... It’d better be true, or that piece shit will get into a lot of trouble…"
    q "Dijo que tenemos algo muy importante de  que hablar... Será mejor que sea verdad, o ese pedazo de  mierda se meterá en un gran problema…"

# game/script.rpy:224
translate spanish start_5ba48b8d:

    # q "To be honest, I don't understand how someone like him managed to reach such a high position. It's repulsive, but most say that his place is very well deserved."
    q "Para ser honesto, no entiendo cómo alguien como él logró alcanzar una posición tan alta. Es repulsivo, pero la mayoría dice que su lugar es muy merecido."

# game/script.rpy:226
translate spanish start_eca1e092:

    # q "A-And just for that reason I decided to give him a few minutes of my valuable time."
    q "Y-Y solo por esa razón decidí darle unos minutos de mi valioso tiempo."

# game/script.rpy:237
translate spanish start_40beda56:

    # q "Compermiso"
    q "Compermiso"

# game/script.rpy:242
translate spanish start_56374d35:

    # m "Hey! Hello please come in..."
    m "¡Hola !, hola por favor entra..."

# game/script.rpy:244
translate spanish start_5770faf9:

    # q "{i}'Fuck, I hate that annoying voice...'{/i}"
    q "{i}'Joder, odio esa voz molesta...'{/i}"

# game/script.rpy:246
translate spanish start_bd16fa77:

    # q "So... do you need me for something?"nointeract
    q "Entonces... ¿me necesitas para algo?"nointeract

# game/script.rpy:259
translate spanish start_A_6125644d:

    # q "Well, can we get started? It had better be something of vital importance. My time is gold, and spending it without reason is an offense that is paid with death..."
    q "Bueno, ¿podemos empezar? Es mejor que sea algo de vital importancia, mi tiempo es oro, y gastarlo sin razón es un delito que se paga con la muerte..."

# game/script.rpy:261
translate spanish start_A_d728a54e:

    # m "Teemo laughs."
    m "Teemo se rie."

# game/script.rpy:263
translate spanish start_A_4097881c:

    # m "I like your sense of humor. Come on, take a seat..."
    m "Me gusta tu sentido del humor, vamos, toma asiento..."

# game/script.rpy:265
translate spanish start_A_cef4e425:

    # q "Sí claro, humor…"
    q "Sí claro, humor…"

# game/script.rpy:267
translate spanish start_A_103aefc5:

    # q "Can you leave the jokes for later? Don't you know that I have better things to do?"
    q "¿Puedes dejar las bromas para después? ¿No sabes que tengo mejores cosas que hacer?"

# game/script.rpy:269
translate spanish start_A_5fee6754:

    # q "Tonight we'll be having an amazing concert, so I have to go to rehearsal!"
    q "Esta noche tendremos un concierto increíble, ¡así que tengo que ir al ensayo!"

# game/script.rpy:271
translate spanish start_A_f05f65d0:

    # m "…"
    m "…"

# game/script.rpy:277
translate spanish start_A_e4ef84b7:

    # q "You aren't listening to me, are you?"
    q "No me estás escuchando, ¿verdad?"

# game/script.rpy:283
translate spanish start_A_8323f02f:

    # q "If you have nothing to say, I'm leaving... I don't have time for your jokes."nointeract
    q "Si no tienes nada que decir, me voy... no tengo tiempo para tus bromas."nointeract

# game/script.rpy:296
translate spanish start_B_0f73fbae:

    # q "Oh come on, not this shit again..."
    q "Oh, vamos, no esta mierda otra vez..."

# game/script.rpy:298
translate spanish start_B_8e12b0b1:

    # q "What do you want!"nointeract
    q "¿Qué quieres?"nointeract

# game/script.rpy:312
translate spanish Act_1_d267661a:

    # q "..."
    q "..."

# game/script.rpy:314
translate spanish Act_1_3dcca5ab:

    # q "Well... th-that's right... A talented one like me doesn't need to attend rehearsal."
    q "Bueno... eso es correcto... Una persona talentosa como yo no necesita asistir al ensayo."

# game/script.rpy:316
translate spanish Act_1_fac39bab:

    # q "Hard work is for those who possess no talent."
    q "El trabajo duro es para aquellos que no poseen talento."

# game/script.rpy:318
translate spanish Act_1_e9be3382:

    # q "{i}'Fucking rat...'{/i}"
    q "{i}'Maldita rata...'{/i}"

# game/script.rpy:320
translate spanish Act_1_f11e12ea:

    # m "That is what I meant! Come on, take a seat and let's talk a little more"
    m "¡A eso me refiero! Vamos, toma asiento y hablemos un poco más"

# game/script.rpy:322
translate spanish Act_1_d754c0f4:

    # q "Tell me, ¿Qué MIERDA quieres?…"
    q "Dime, ¿Qué MIERDA quieres?..."

# game/script.rpy:324
translate spanish Act_1_c7810792:

    # m "Everyday I get a lot of mail from angry fans. They are very upset about your behavior..."
    m "Todos los días recibo muchos correos de fanáticos enojados, están muy molestos por tu comportamiento..."

# game/script.rpy:330
translate spanish Act_1_98c881a3:

    # q "Then, aswer every mail with {b}'FUCK YOU, The princess Qiyana is not going to change for bunch of haters´{/b}"nointeract
    q "Entonces, responde cada correo con {b}'JODETE, la princesa Qiyana no va a cambiar por un montón de haters´{/b}"nointeract

# game/script.rpy:343
translate spanish Act_1_A_7bfc04b1:

    # q "wh-what?..."
    q "¿Q-QUÉ?..."

# game/script.rpy:349
translate spanish Act_1_A_12b318e5:

    # m "And I know a good way to clean that pretty mouth, miss."
    m "Y sé una buena manera de limpiar esa bonita boca, señorita."

# game/script.rpy:351
translate spanish Act_1_A_d728a54e:

    # m "Teemo laughs."
    m "Teemo se rie."

# game/script.rpy:357
translate spanish Act_1_A_0d45746c:

    # q "Tch... I ain't gonna suck your dick, stupid rat... N-not again..."
    q "Tch... no voy a chuparte la polla, estúpida rata... N-no otra vez..."

# game/script.rpy:359
translate spanish Act_1_A_9d144aed:

    # q "Someone like me with a fucking rat like you. ¿ESTÁS LOCO?"
    q "Alguien como yo con una jodida rata como tú. ¿ESTÁS LOCO?"

# game/script.rpy:361
translate spanish Act_1_A_dc8f498b:

    # m "Wow, that hurt."
    m "WOW, eso dolió."

# game/script.rpy:372
translate spanish Act_1_A_efe1b77c:

    # n "...And Teemo Manager ripped his clothes with his muscles and shows Qiyana his 30 cm combat blowgun"
    n "...Y el Manager teemo se rimpió la ropa con los músculos y le muestra a Qiyana su cerbatana de combate de 30 cm"

# game/script.rpy:374
translate spanish Act_1_A_a8b7b480:

    # q "L-Let´s see..."
    q "Veamos…"

# game/script.rpy:380
translate spanish Act_1_A_1a8a9d01:

    # q "O-Ok, just a little bit..."
    q "O-Ok, solo un poco..."

# game/script.rpy:382
translate spanish Act_1_A_318be34c:

    # q "...But don't think I'll do it because I like it!"
    q "... ¡Pero no pienses que lo haré porque me gusta!"

# game/script.rpy:384
translate spanish Act_1_A_d728a54e_1:

    # m "Teemo laughs."
    m "Teemo se rie."

# game/script.rpy:386
translate spanish Act_1_A_bd3fd6be:

    # m "Sure sure, you have nothing to fear. I don't think this will leave you speechless, or at least, that's what I think..."
    m "Claro seguro, no tienes nada que temer, no creo que esto te deje muda, o eso es lo que creo..."

# game/script.rpy:394
translate spanish Act_1_A_31504f7a:

    # m "Do you think you can handle this again?"
    m "¿Crees que podrás manejar esto de nuevo?"

# game/script.rpy:396
translate spanish Act_1_A_6d9de956:

    # q "Hahaha, don't make me laugh..."
    q "Jajaja, no me hagas reír..."

# game/script.rpy:398
translate spanish Act_1_A_c41fd9ab:

    # q "C-Come on, you're not that big..."
    q "Va-Vamos, no eres tan grande..."

# game/script.rpy:400
translate spanish Act_1_A_95ebb592:

    # q "{i}'This reminds me my beginnings... Those days were the best.'{/i}"
    q "{i}' Esto me recuerda mis comienzos... Esos días fueron los mejores.'{/i}"

# game/script.rpy:402
translate spanish Act_1_A_dbb1ae2d:

    # q "{i}'Damn Qiyana, What the fuck are you thinking about!'{/i}"
    q "{i}'Maldición Qiyana, ¿en qué demonios estás pensando?'{/i}"

# game/script.rpy:404
translate spanish Act_1_A_0ea13bd6:

    # q "{i}'You are going to suck t-that thick piece meat again...'{/i}"
    q "{i}'Vas a chupar esa carne gruesa de nuevo...'{/i}"

# game/script.rpy:406
translate spanish Act_1_A_63a4bd89:

    # q "{i}'That juicy thick piece of meat...'{/i}"
    q "{i}'Ese jugoso y grueso trozo de carne...'{/i}"

# game/script.rpy:408
translate spanish Act_1_A_637c16a3:

    # q "{i}'Ok, I'm horny, fuck this shit!'{/i}"
    q "{i}'Ok, estoy cachonda, ¡a la mierda esta esto!'{/i}"

# game/script.rpy:410
translate spanish Act_1_A_d5e57a32:

    # m "Come on, show me your tattoo..."
    m "Vamos, muéstrame tu tatuaje .."

# game/script.rpy:412
translate spanish Act_1_A_0e4bc5c4:

    # q "…"
    q "…"

# game/script.rpy:414
translate spanish Act_1_A_c22a154f:

    # q "You will pay for this..."
    q "Pagarás por esto..."

# game/script.rpy:419
translate spanish Act_1_A_0e4bc5c4_1:

    # q "…"
    q "…"

# game/script.rpy:421
translate spanish Act_1_A_6c66d5cb:

    # n "...And Teemo Manager puts his tremendous combat blowgun on the face of an angry but horny Qiyana"
    n "... Y el manager Teemo pone su tremenda pistola de combate en la cara de una enojada pero cachonda Qiyana"

# game/script.rpy:426
translate spanish Act_1_A_d728a54e_2:

    # m "Teemo laughs."
    m "Teemo se rie."

# game/script.rpy:428
translate spanish Act_1_A_981872c4:

    # q "S-Show some respect! Piece of trash..."
    q "Mu-Muestra algo de respeto! Pedazo de basura..."

# game/script.rpy:430
translate spanish Act_1_A_b38c635e:

    # m "Sorry you look so hot from this angle and It doesn't help at all..."
    m "Lo siento, te ves tan sexy desde este ángulo y no ayuda en absoluto..."

# game/script.rpy:432
translate spanish Act_1_A_b3b2020e:

    # q "C'mon, what are you waiting for?! I don't have a lot of time!"
    q "Vamos, ¿qué estás esperando? ¡No tengo mucho tiempo!"

# game/script.rpy:434
translate spanish Act_1_A_a019bee3:

    # m "Wow, really? Let me enjoy this a little..."
    m "Wow, de verdad? Déjame disfrutar esto un poco... "

# game/script.rpy:436
translate spanish Act_1_A_9b94a971:

    # q "You do know that I'm a princess, right?"
    q "Sabes que soy una princesa, ¿verdad?"

# game/script.rpy:438
translate spanish Act_1_A_5f36edf9:

    # m "Sure your highness..."
    m "Seguro, su alteza..."

# game/script.rpy:440
translate spanish Act_1_A_ab842d91:

    # q "No, I'm better than that. I'm the future Empress Qi..."
    q "No, soy mejor que eso. Soy la futura Emperatriz Qi..."

# game/script.rpy:451
translate spanish Act_1_A_e8701249:

    # m "Yeah, of course... Now shut up and suck it."
    m "Sí, por supuesto... Ahora cállate y chúpalo."

# game/script.rpy:457
translate spanish Act_1_A_fdbee13b:

    # q "Don't you know what it is to be gentle? [dialog with the mouth full]"
    q "¿No sabes lo que es ser gentil? [diálogo con la boca llena]"

# game/script.rpy:459
translate spanish Act_1_A_1592acc4:

    # m "Does that really matter?"
    m "¿Eso realmente importa?"

# game/script.rpy:461
translate spanish Act_1_A_cf3b4795:

    # m "You are sucking it like a champ."
    m "Lo estás chupando como toda un campeona."

# game/script.rpy:465
translate spanish Act_1_A_4089a09c:

    # q "{i}'T-This amazing feeling, I can feel how it runs down my throat, warms my chest, and makes me tremble'{/i}"
    q "{i}'E-Esta increíble sensación, puedo sentir cómo me baja por la garganta, me calienta el pecho y me hace temblar...'{/i}"

# game/script.rpy:467
translate spanish Act_1_A_a7b63f2d:

    # q "{i}'Fuck, I'm losing it...'{/i}"
    q "{i}'Joder, estoy enloqueciendo...'{/i}"

# game/script.rpy:471
translate spanish Act_1_A_e1d214c7:

    # q "{i}'I never thought that a guy like this would make me feel something like this...'{/i}"
    q "{i}'Nunca pensé que un tipo como este me haría sentir algo así ..'{/i}"

# game/script.rpy:473
translate spanish Act_1_A_f418683d:

    # q "{i}'From the day I arrived at the group and saw him, I knew he would be a nuisance...'{/i}"
    q "{i}'Desde el día en que llegué al grupo y lo vi, supe que sería una molestia...'{/i}"

# game/script.rpy:475
translate spanish Act_1_A_9f6f60a3:

    # q "{i}'I-I can feel it, that taste in my throat... And I must admit it, is very good.'{/i}"
    q "{i}'Puedo sentirlo, ese sabor en la garganta... Y debo admitir, es muy bueno'{/i}"

# game/script.rpy:479
translate spanish Act_1_A_ee79fa04:

    # m "Fuuuck, I think I'm going to cum!.. And is a really big load."
    m "¡MIEERDA, creo que me voy a correr!... Y es una carga realmente grande."

# game/script.rpy:481
translate spanish Act_1_A_c030f712:

    # m "Prepare yourself, your highness..."
    m "Prepárate, alteza..."

# game/script.rpy:485
translate spanish Act_1_A_a9bdf76a:

    # q "HMMM HMMM HMM"
    q "HMMM HMMM HMM"

# game/script.rpy:487
translate spanish Act_1_A_5e43ce06:

    # m "Darling, darling, darling, we've already talked about speaking with your mouth full..."
    m "Querida, querida, querida, ya hemos hablado de hablar con la boca llena..."

# game/script.rpy:489
translate spanish Act_1_A_de05cbdc:

    # m "Try to be clearer, please."
    m "Intenta ser más claro, por favor. "

# game/script.rpy:491
translate spanish Act_1_A_d728a54e_3:

    # m "Teemo laughs."
    m "Teemo se rie."

# game/script.rpy:493
translate spanish Act_1_A_0559175b:

    # m "C'mom swallow it..."
    m "Vamos, trágualo..."

# game/script.rpy:533
translate spanish Act_1_B_41a0e1d3:

    # m "W-Wait, there a little more..."
    m "E-Espera, hay un poco más..."

# game/script.rpy:538
translate spanish Act_1_B_aca4fc73:

    # q "HMMM!"
    q "HMMM!"

# game/script.rpy:544
translate spanish Act_1_B_c886c46a:

    # q "HHHMMMMM!"
    q "HHHMMMMM!"

# game/script.rpy:550
translate spanish Act_1_B_ea471eaf:

    # q "{b}HHHMMMMM!{/b}"
    q "{b}HHHMMMMM!{/b}"

# game/script.rpy:557
translate spanish Act_1_B_15812363:

    # m "Good job, your highness..."
    m "Buen trabajo, su alteza..."

# game/script.rpy:568
translate spanish Act_1_B_7a24f555:

    # q "D-Don't even think about doing that shit on my face again!"
    q "Ni-¡Ni siquiera pienses en volver a hacer esa mierda en mi cara!"

# game/script.rpy:570
translate spanish Act_1_B_504a4c11:

    # q "I don't want you to dirty me with that filthy shit..."
    q "No quiero que me ensucies con esa mierda sucia..."

# game/script.rpy:572
translate spanish Act_1_B_b4164d54:

    # q "{i}'Fuck I just came sucking his dirty dick, the idea of get loads of his dirty stuff on my face turn me on...'{/i}"
    q "{i}'Mierda, le  acabo de chuparle su horrible pene sucio, la idea de sentir toda esa cosa sucia y caliente en la cara me excita...'{/i}"

# game/script.rpy:574
translate spanish Act_1_B_6b440128:

    # m "C-Come on don't ruin the moment... This is only the beginning..."
    m "Va-Vamos, no arruines el momento... Esto es solo el comienzo... "

translate spanish strings:

    # game/script.rpy:1
    old "Qiyana"
    new "Qiyana"

    # game/script.rpy:2
    old "Teemo"
    new "Teemo"

    # game/script.rpy:246
    old "Yes, lady!"
    new "¡Sí, señorita!"

    # game/script.rpy:246
    old "SHUT UP AND SUCK MY DICK!"
    new "¡CIERRA LA BOCA Y CHUPAME LA POLLA!"

    # game/script.rpy:283
    old "Are you leaving? Too bad, seriously, I was very anxious to show you someting..."
    new "¿Ya te vas? Que mal, en serio, Estaba muy ansioso de mostrarte algo..."

    # game/script.rpy:298
    old "I want to show you my porn collection of Ahri being fucked in the ass... TOP NOTCH material."
    new "Te quiero mostrar mi colección de porno de Ahri siengo follada por el culo... MATERIAL DEL BUENO."

    # game/script.rpy:298
    old "You don't need to attend rehearsal. The natural talent of the princess Qiyana is sufficient..."
    new "No tienes que ir al ensayo. El talento natural de la princesa Qiyana es suficiente..."

    # game/script.rpy:330
    old "Hey hey hey, that vocabulary is something we should change, don't you think?"
    new "Hey hey hey, Ese vocabulario es algo que debemos cambiar, ¿No lo crees?"

    # game/script.rpy:330
    old "OK, I going to send that mail to everyone. Sorry for the inconveniences."
    new "OK, Enviaré un correo a todos. Perdón por las inconveniencias."

    # game/script.rpy:508
    old "Keep fucking her mouth!"
    new "¡Sigue follando su boca!"

    # game/script.rpy:508
    old "CUM!"
    new "¡CÓRRETE!"

# TODO: Translation updated at 2020-04-28 01:38

translate spanish strings:

    # game/script.rpy:289
    old "I want to show you my porn collection of Ahri being fucked in the ass ... TOP NOTCH material."
    new "Te quiero mostrar mi colección de porno de Ahri siengo follada por el culo... MATERIAL DEL BUENO."

