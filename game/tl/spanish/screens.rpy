﻿# TODO: Translation updated at 2020-04-26 21:33

translate spanish strings:

    # game/screens.rpy:256
    old "Back"
    new "Volver"

    # game/screens.rpy:257
    old "History"
    new "Historia"

    # game/screens.rpy:258
    old "Skip"
    new "Saltar"

    # game/screens.rpy:259
    old "Auto"
    new "Auto"

    # game/screens.rpy:260
    old "Save"
    new "Guardar"

    # game/screens.rpy:261
    old "Q.Save"
    new "Guardado R."

    # game/screens.rpy:262
    old "Cargado R."
    new ""

    # game/screens.rpy:263
    old "Prefs"
    new "Opciones"

    # game/screens.rpy:304
    old "Start"
    new "Jugar"

    # game/screens.rpy:312
    old "Load"
    new "Cargar"

    # game/screens.rpy:314
    old "Preferences"
    new "Opciones"

    # game/screens.rpy:318
    old "End Replay"
    new "Terminar Replay"

    # game/screens.rpy:322
    old "Main Menu"
    new "Menú Principal"

    # game/screens.rpy:324
    old "About"
    new "Créditos"

    # game/screens.rpy:329
    old "Help"
    new "Ayuda"

    # game/screens.rpy:335
    old "Quit"
    new "Salir"

    # game/screens.rpy:476
    old "Return"
    new "Volver"

    # game/screens.rpy:560
    old "Version [config.version!t]\n"
    new "Versión [config.version!t]\n"

    # game/screens.rpy:566
    old "Made with {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]"
    new "Made with {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]"

    # game/screens.rpy:606
    old "Page {}"
    new "Página {}"

    # game/screens.rpy:606
    old "Automatic saves"
    new "Guardados Automático"

    # game/screens.rpy:606
    old "Quick saves"
    new "Guardados Rápidos"

    # game/screens.rpy:648
    old "{#file_time}%A, %B %d %Y, %H:%M"
    new "{#file_time}%A, %B %d %Y, %H:%M"

    # game/screens.rpy:648
    old "empty slot"
    new "Espacio Vacío"

    # game/screens.rpy:665
    old "<"
    new "<"

    # game/screens.rpy:668
    old "{#auto_page}A"
    new "{#auto_page}A"

    # game/screens.rpy:671
    old "{#quick_page}Q"
    new "{#quick_page}Q"

    # game/screens.rpy:677
    old ">"
    new ">"

    # game/screens.rpy:734
    old "Display"
    new "Pantalla"

    # game/screens.rpy:735
    old "Window"
    new "Ventana"

    # game/screens.rpy:736
    old "Fullscreen"
    new "Ventana Completa"

    # game/screens.rpy:740
    old "Rollback Side"
    new "Retroceso"

    # game/screens.rpy:741
    old "Disable"
    new "Deshabilitar"

    # game/screens.rpy:742
    old "Left"
    new "Izquierda"

    # game/screens.rpy:743
    old "Right"
    new "Derecha"

    # game/screens.rpy:748
    old "Unseen Text"
    new "Texto No Visto"

    # game/screens.rpy:749
    old "After Choices"
    new "Después de Decisiones"

    # game/screens.rpy:750
    old "Transitions"
    new "Transiciones"

    # game/screens.rpy:763
    old "Text Speed"
    new "Velocidad de Texto"

    # game/screens.rpy:767
    old "Auto-Forward Time"
    new "Auto-Forward Time"

    # game/screens.rpy:774
    old "Music Volume"
    new "Volumen de Música"

    # game/screens.rpy:781
    old "Sound Volume"
    new "Volumen de Sonidos"

    # game/screens.rpy:787
    old "Test"
    new "Prueba"

    # game/screens.rpy:791
    old "Voice Volume"
    new "Volumen de Voz"

    # game/screens.rpy:802
    old "Mute All"
    new "Silenciar a Todos"

    # game/screens.rpy:921
    old "The dialogue history is empty."
    new "El historial de diálogo esta vacío."

    # game/screens.rpy:991
    old "Keyboard"
    new "Teclado"

    # game/screens.rpy:992
    old "Mouse"
    new "Ratón"

    # game/screens.rpy:995
    old "Gamepad"
    new "Gamepad"

    # game/screens.rpy:1008
    old "Enter"
    new "Enter"

    # game/screens.rpy:1009
    old "Advances dialogue and activates the interface."
    new "Avanza el diálogo y activa la interfaz."

    # game/screens.rpy:1012
    old "Space"
    new "Espacio"

    # game/screens.rpy:1013
    old "Advances dialogue without selecting choices."
    new "Avanza el diálogo sin seleccionar opciones."

    # game/screens.rpy:1016
    old "Arrow Keys"
    new "Direccionales"

    # game/screens.rpy:1017
    old "Navigate the interface."
    new "Navega la interfáz."

    # game/screens.rpy:1020
    old "Escape"
    new "Escape"

    # game/screens.rpy:1021
    old "Accesses the game menu."
    new "Accede al menú del juego."

    # game/screens.rpy:1024
    old "Ctrl"
    new "Ctrl"

    # game/screens.rpy:1025
    old "Skips dialogue while held down."
    new "Omite el diálogo mientras se mantiene presionada."

    # game/screens.rpy:1028
    old "Tab"
    new "Tab"

    # game/screens.rpy:1029
    old "Toggles dialogue skipping."
    new "Alterna el diálogo saltando."

    # game/screens.rpy:1032
    old "Page Up"
    new "Page Up"

    # game/screens.rpy:1033
    old "Rolls back to earlier dialogue."
    new "Retrocede al diálogo anterior."

    # game/screens.rpy:1036
    old "Page Down"
    new "Página Abajo."

    # game/screens.rpy:1037
    old "Rolls forward to later dialogue."
    new "Avanza al diálogo siguiente."

    # game/screens.rpy:1041
    old "Hides the user interface."
    new "Oculta la interfaz de usuario."

    # game/screens.rpy:1045
    old "Takes a screenshot."
    new "Tomar captura de pantalla."

    # game/screens.rpy:1049
    old "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."
    new "Alterna asistencia {a=https://www.renpy.org/l/voicing}self-voicing{/a}."

    # game/screens.rpy:1055
    old "Left Click"
    new "Clic Izquierdo"

    # game/screens.rpy:1059
    old "Middle Click"
    new "Clic Medio"

    # game/screens.rpy:1063
    old "Right Click"
    new "Clic Derecho"

    # game/screens.rpy:1067
    old "Mouse Wheel Up\nClick Rollback Side"
    new "Rueda del Ratón\nClic Rollback Side"

    # game/screens.rpy:1071
    old "Mouse Wheel Down"
    new "Rueda de Ratón Hacia Abajo"

    # game/screens.rpy:1078
    old "Right Trigger\nA/Bottom Button"
    new "Gatillo Derecho\nA/Botón Inferior"

    # game/screens.rpy:1082
    old "Left Trigger\nLeft Shoulder"
    new "Gatillo Izquierdo\nHombro Izquierdo"

    # game/screens.rpy:1086
    old "Right Shoulder"
    new "Hombro Derecho"

    # game/screens.rpy:1091
    old "D-Pad, Sticks"
    new "D-Pad, Sticks"

    # game/screens.rpy:1095
    old "Start, Guide"
    new "Start, Guía"

    # game/screens.rpy:1099
    old "Y/Top Button"
    new "Y/Botón Superior"

    # game/screens.rpy:1102
    old "Calibrate"
    new "Calibrar"

    # game/screens.rpy:1167
    old "Yes"
    new "Si"

    # game/screens.rpy:1168
    old "No"
    new "No"

    # game/screens.rpy:1214
    old "Skipping"
    new "Saltando"

    # game/screens.rpy:1437
    old "Menu"
    new "Menú"

# TODO: Translation updated at 2020-04-26 22:58

translate spanish strings:

    # game/screens.rpy:262
    old "Q.Load"
    new "Cargado R."

    # game/screens.rpy:757
    old "Language"
    new "Idioma"

