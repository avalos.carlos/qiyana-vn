define q = Character(_("Qiyana"), color="#EB5DFF" , who_outlines=[ (2, "#000000") ], what_outlines=[ (2, "#000000")])
define m = Character(_("Teemo"), color="#E8B03B"  , who_outlines=[ (2, "#000000") ], what_outlines=[ (2, "#000000")])

define n = Character("", color="#ffffff", who_outlines=[ (2, "#000000") ], what_outlines=[ (2, "#000000")])

image angry quiyana = LiveComposite(
  (725, 1080),
  (0, 0), "ch q angry c.png",
  (0, 0), "angry eyes quiyana",
)

image disgusted quiyana = LiveComposite(
  (725, 1080),
  (0, 0), "ch q disgusted c.png",
  (0, 0), "disgusted eyes quiyana",
)

image horny quiyana = LiveComposite(
  (725, 1080),
  (0, 0), "ch q horny c.png",
  (0, 0), "horny eyes quiyana",
)

image surprised quiyana = LiveComposite(
  (725, 1080),
  (0, 0), "ch q surprised c.png",
  (0, 0), "surprised eyes quiyana",
)

image cum quiyana = LiveComposite(
  (725, 1080),
  (0, 0), "ch q cum c.png",
  (0, 0), "cum eyes quiyana",
)

image angry eyes quiyana:
  "angry eyes.png"
  choice:
    4.5
  choice:
    3.5
  choice:
    1.5# This randomizes the time between blinking.
  "eyes closed.png"
  .25
  repeat
  
image disgusted eyes quiyana:
  "disgusted eyes.png"
  choice:
    4.5
  choice:
    3.5
  choice:
    1.5# This randomizes the time between blinking.
  "eyes closed.png"
  .25
  repeat
  
image horny eyes quiyana:
  "horny eyes.png"
  choice:
    4.5
  choice:
    3.5
  choice:
    1.5# This randomizes the time between blinking.
  "eyes closed.png"
  .25
  repeat
  
image surprised eyes quiyana:
  "surprised eyes.png"
  choice:
    4.5
  choice:
    3.5
  choice:
    1.5# This randomizes the time between blinking.
  "eyes closed.png"
  .25
  repeat

image cum eyes quiyana:
  "cum eyes.png"
  choice:
    4.5
  choice:
    3.5
  choice:
    1.5# This randomizes the time between blinking.
  "eyes closed.png"
  .25
  repeat




# The game starts here.
#Act 0 : character develop
#Act 1 : "forced" bowjob
#Act 2 : anal doggy style
#Act 3 : anal prone bone legs spread
#Act 4 : anal full nelson
#Act 5 : anal mating press



label oral_secuence:
    
    $oralono = [ "HMM!!!","{i}'FUCK'{/i}","HMMM!!","HHHMMM!!!" ]
    
    show ch oral head
    with dissolve
    
    $q(renpy.random.choice(oralono))
    
    show ch oral med
    with dissolve
    
    $q(renpy.random.choice(oralono))
    
    show ch oral base
    with vpunch
    
    $q(renpy.random.choice(oralono))
    
    return
    
label oral_finisher(v = 0.060):

    show ch oral hands00
    $renpy.pause(v, hard=True)
    show ch oral hands01
    with vpunch
    
    return

  
label basic_defeat:
    
    scene bg gameover01
    with vpunch
    
    q "FUCKING RAT!!!"

    q "I should never have come here..."
    
    n "{b}GAME OVER{/b}"
    
    return

label start:

    #Act 0 : character develop

    #play music "background.ogg"
    
    scene bg group
    with fade
    
    #play sound "public_cheers.ogg"
    
    q "After the worldwide success that was K/Da, Akali decided to search and recruit the best musicians to form a new group, True Damage."
    
    q "I have to admit that Akali, Ekko, Senna and Yasuo are quite talented artists but, you know, they’re nothing without me, la hermosa princesa Qiyana."
    
    #stop sound "public_cheers.ogg"
    
    show photo01
    with zoomin
    $renpy.pause()
    
    show photo02
    with zoomin
    $renpy.pause()
    
    show photo03
    with zoomin
    $renpy.pause()
    
    show photo04
    with zoomin
    $renpy.pause()
    
    q "And as you expected, I'm not here to talk about wasted talents, I'm here to talk about me, a goddess among you."
    
    scene bg hallway
    with fade
    
    show ch q walk01
    with moveinright
    
    q "Our manager, Teemo, an irrelevant creep. I really hate that piece of shit..."
    
    show tbody01 at topleft
    with moveinleft
    
    q "I have no time to explain why I’m still holding up that guy..."
    
    hide tbody01
    show tbody02 at topleft
    with vpunch
    
    q "..."

    hide tbody02
    
    hide ch q walk01
    show ch q walk02
    with dissolve
    
    q "He said we have something very important to talk about... It’d better be true, or that piece shit will get into a lot of trouble…"
    
    q "To be honest, I don't understand how someone like him managed to reach such a high position. It's repulsive, but most say that his place is very well deserved."
    
    q "A-And just for that reason I decided to give him a few minutes of my valuable time."
    
    
    #play sound "open_door.ogg"
    
    scene bg t office
    with fade
    
    show angry quiyana at right
    with moveinright
 
    q "Compermiso"

    show ch_teemo01_c at left
    with moveinleft
    
    m "Hey! Hello please come in..."

    q "{i}'Fuck, I hate that annoying voice...'{/i}"
    
    menu:

        q "So... do you need me for something?"

        "Yes, lady!":
            jump start_A

        "SHUT UP AND SUCK MY DICK!":
            jump basic_defeat


label start_A:

    q "Well, can we get started? It had better be something of vital importance. My time is gold, and spending it without reason is an offense that is paid with death..."

    m "Teemo laughs."
    
    m "I like your sense of humor. Come on, take a seat..."
    
    q "Sí claro, humor…"
    
    q "Can you leave the jokes for later? Don't you know that I have better things to do?"
    
    q "Tonight we'll be having an amazing concert, so I have to go to rehearsal!"

    m "…"
    
    hide angry quiyana
    show disgusted quiyana at right
    with dissolve

    q "You aren't listening to me, are you?"

    hide disgusted quiyana
    show angry quiyana at right
    with dissolve

    menu:
    
        q "If you have nothing to say, I'm leaving... I don't have time for your jokes."

        "Yes, lady!":
            jump basic_defeat

        "Are you leaving? Too bad, seriously, I was very anxious to show you someting...":
            jump start_B

    
label start_B:

    q "Oh come on, not this shit again..."
    
    menu:
    
        q "What do you want!"

        "I want to show you my porn collection of Ahri being fucked in the ass ... TOP NOTCH material.":
            jump basic_defeat

        "You don't need to attend rehearsal. The natural talent of the princess Qiyana is sufficient...":
            jump Act_1
    
label Act_1:
    
    #Act 1 : "forced" bowjob

    q "..."

    q "Well... th-that's right... A talented one like me doesn't need to attend rehearsal."

    q "Hard work is for those who possess no talent."

    q "{i}'Fucking rat...'{/i}"

    m "That is what I meant! Come on, take a seat and let's talk a little more"

    q "Tell me, ¿Qué MIERDA quieres?…"

    m "Everyday I get a lot of mail from angry fans. They are very upset about your behavior..."
    
    hide angry quiyana
    show disgusted quiyana at right
    with dissolve

    menu:
    
        q "Then, aswer every mail with {b}'FUCK YOU, The princess Qiyana is not going to change for bunch of haters´{/b}"

        "Hey hey hey, that vocabulary is something we should change, don't you think?":
            jump Act_1_A
        
        "OK, I going to send that mail to everyone. Sorry for the inconveniences.":
            jump basic_defeat


label Act_1_A:
    
    q "wh-what?..."
    
    hide disgusted quiyana
    show angry quiyana at right
    with dissolve

    m "And I know a good way to clean that pretty mouth, miss."
    
    m "Teemo laughs."
    
    hide angry quiyana
    show disgusted quiyana at right
    with dissolve

    q "Tch... I ain't gonna suck your dick, stupid rat... N-not again..."

    q "Someone like me with a fucking rat like you. ¿ESTÁS LOCO?"

    m "Wow, that hurt."

    #play sound "ripping clothes.ogg"
    hide ch_teemo01_c
    show ch_teemo02_c at left
    with vpunch
    
    hide disgusted quiyana
    show surprised quiyana at right
    with dissolve
    
    n "...And Teemo Manager ripped his clothes with his muscles and shows Qiyana his 30 cm combat blowgun"
    
    q "L-Let´s see..."
    
    hide surprised quiyana
    show horny quiyana at right
    with dissolve

    q "O-Ok, just a little bit..."
    
    q "...But don't think I'll do it because I like it!"
    
    m "Teemo laughs."
    
    m "Sure sure, you have nothing to fear. I don't think this will leave you speechless, or at least, that's what I think..."
    
    scene bg floor
    with fade
    
    show ch oral
    with dissolve
    
    m "Do you think you can handle this again?"
    
    q "Hahaha, don't make me laugh..."
    
    q "C-Come on, you're not that big..."
    
    q "{i}'This reminds me my beginnings... Those days were the best.'{/i}"
    
    q "{i}'Damn Qiyana, What the fuck are you thinking about!'{/i}"

    q "{i}'You are going to suck t-that thick piece meat again...'{/i}"

    q "{i}'That juicy thick piece of meat...'{/i}"
    
    q "{i}'Ok, I'm horny, fuck this shit!'{/i}"

    m "Come on, show me your tattoo..."
    
    q "…"
    
    q "You will pay for this..."
    
    show ch oral tongue
    with dissolve
    
    q "…"
    
    n "...And Teemo Manager puts his tremendous combat blowgun on the face of an angry but horny Qiyana"
    
    show ch oral dickface
    with vpunch

    m "Teemo laughs."

    q "S-Show some respect! Piece of trash..."
    
    m "Sorry you look so hot from this angle and It doesn't help at all..."

    q "C'mon, what are you waiting for?! I don't have a lot of time!"

    m "Wow, really? Let me enjoy this a little..."

    q "You do know that I'm a princess, right?"

    m "Sure your highness..."
    
    q "No, I'm better than that. I'm the future Empress Qi..."
    
    show ch oral head
    $renpy.pause(0.125, hard=True)
    hide ch oral head
    show ch oral med
    $renpy.pause(0.125, hard=True)
    hide ch oral med
    show ch oral base
    with vpunch

    m "Yeah, of course... Now shut up and suck it."
    
    call oral_secuence from _call_oral_secuence
    call oral_secuence from _call_oral_secuence_1
    call oral_secuence from _call_oral_secuence_2
    
    q "Don't you know what it is to be gentle? [dialog with the mouth full]"
    
    m "Does that really matter?"
    
    m "You are sucking it like a champ."
    
    call oral_secuence from _call_oral_secuence_3
    
    q "{i}'T-This amazing feeling, I can feel how it runs down my throat, warms my chest, and makes me tremble'{/i}"
    
    q "{i}'Fuck, I'm losing it...'{/i}"
    
    call oral_secuence from _call_oral_secuence_4
    
    q "{i}'I never thought that a guy like this would make me feel something like this...'{/i}"

    q "{i}'From the day I arrived at the group and saw him, I knew he would be a nuisance...'{/i}"
    
    q "{i}'I-I can feel it, that taste in my throat... And I must admit it, is very good.'{/i}"

    call oral_secuence from _call_oral_secuence_5
    
    m "Fuuuck, I think I'm going to cum!.. And is a really big load."
    
    m "Prepare yourself, your highness..."
    
    call oral_secuence from _call_oral_secuence_6
    
    q "HMMM HMMM HMM"
    
    m "Darling, darling, darling, we've already talked about speaking with your mouth full..."
    
    m "Try to be clearer, please."

    m "Teemo laughs."

    m "C'mom swallow it..."
 
label Act_1_A_End:
    $count = 10
    while count > 0:
        call oral_finisher() from _call_oral_finisher
        $count -= 1
    
    menu:

        "Keep fucking her mouth!":
            jump Act_1_A_End
        
        "CUM!":
            jump Act_1_B    
    
    
label Act_1_B:
    $count = 10
    while count > 0:
        call oral_finisher(0.030) from _call_oral_finisher_1
        $count -= 1
 
    show ch oral hands02
    with vpunch
    
    m "W-Wait, there a little more..."
    
    show ch oral hands03
    with vpunch
    
    q "HMMM!"

    
    show ch oral hands03 
    with vpunch
    
    q "HHHMMMMM!"
    
    
    show ch oral hands03 
    with vpunch
    
    q "{b}HHHMMMMM!{/b}"
    
    
    show ch oral cum03 
    with dissolve
    $renpy.pause()
    
    m "Good job, your highness..."
    
    scene bg t office
    with fade
    
    show cum quiyana at right
    with moveinright
    
    show ch_teemo02_c at left
    with moveinleft
    
    q "D-Don't even think about doing that shit on my face again!"
    
    q "I don't want you to dirty me with that filthy shit..."
    
    q "{i}'Fuck I just came sucking his dirty dick, the idea of get loads of his dirty stuff on my face turn me on...'{/i}"
    
    m "C-Come on don't ruin the moment... This is only the beginning..."
    
    #scene bg cred
    #$renpy.pause()
    
    jump Act_2